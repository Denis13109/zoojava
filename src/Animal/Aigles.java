package Animal;

public class Aigles extends Animal implements AnimalVolant {

    public Aigles(String name, char sex, int weight, int size) {
        super(name, sex, weight, size);
    }

    public void fly() {
        System.out.println(super.getName() + " et il s'envole, il s'envole, il s'envole !!!!");
    }
}