package Animal;

public abstract class Animal {
    private String name;
    private char sex; // sexe m : male, f : female
    private int weight; // poids en Kg
    private int size; // taille en cm
    private boolean hunger; // si false = n'a pas faim -- true = afamm�
    private boolean sleep; // si false = ne dort pas -- true = est entrain de dormir
    private boolean health; // si false = en mauvaise sant� -- true = en bonne sant�

    public Animal(String name, char sex, int weight, int size) {
        this.name = name;
        setSex(sex);
        this.weight = weight;
        this.size = size;
        this.hunger = false;
        this.sleep = false;
        this.health = true;
    }

    public void eat() {
        if(this.hunger) {
            this.hunger = false;
            System.out.println(this.name + " est en train de manger");
        }
    }

    public void sound() {
        System.out.println(this.name + " crie");
    }

    public void heal() {
        if(!this.health) {
            this.health = true;
        }
    }

    public void giveBirth() {
        if(this.sex == 'f') {
            System.out.println(this.name + " pond des oeufs");
        }
    }

    public void sleep() {
        if(!this.sleep) {
            this.sleep = true;
            System.out.println(this.name + " s'endort et rejoins le monde des r�ves");
        }
    }

    public void wakeUp() {
        if(this.sleep) {
            this.sleep = false;
            System.out.println(this.name + " se r�veille !");
        }
    }

    public String getName() {
        return name;
    }

    public char getSex() {
        return sex;
    }

    public void setName(String name) {
        this.name = name;
    }

    private void setSex(char sex) {
        if(sex == 'm' || sex == 'f') {
            this.sex = sex;
        } else {
            System.out.println("Le sexe doit �tre soit 'm'(male) soit 'f'(femelle) !");
        }
    }

    public boolean isSleep() {
        return sleep;
    }

    public boolean isHealth() {
        return health;
    }

    @Override
    public String toString() {
        return "Animal{" +
                "name='" + name + '\'' +
                ", sex=" + sex +
                ", weight=" + weight +
                ", size=" + size +
                ", hunger=" + hunger +
                ", sleep=" + sleep +
                ", health=" + health +
                '}';
    }
}