package Animal;

public class Baleines extends Mamifere implements AnimalMarin {
    public Baleines(String name, char sex, int weight, int size) {
        super(name, sex, weight, size);
    }

    public void swim() {
        System.out.println(super.getName() + " nage ...");
    }
}