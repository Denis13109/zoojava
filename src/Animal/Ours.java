package Animal;

public class Ours extends Mamifere implements AnimalTerrestre {

    public Ours(String name, char sex, int weight, int size) {
        super(name, sex, weight, size);
    }

    public void walk() {
        System.out.println(super.getName() + " vagabonne !");
    }
}