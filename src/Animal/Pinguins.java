package Animal;

public class Pinguins extends Animal implements AnimalTerrestre {

    public Pinguins(String name, char sex, int weight, int size) {
        super(name, sex, weight, size);
    }

    public void walk() {
        System.out.println(super.getName() + " se balade trkl !");
    }
}