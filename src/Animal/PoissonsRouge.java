package Animal;

public class PoissonsRouge extends Animal implements AnimalMarin {

    public PoissonsRouge(String name, char sex, int weight, int size) {
        super(name, sex, weight, size);
    }

    public void swim() {
        System.out.println(super.getName() + " nage!");
    }
}
