package Employe;

import Animal.Animal;
import Enclot.Enclots;

// Design Pattern : Singleton
public final class Employes {
    private String name;
    private char sex;
    private int age;
    private static Employes employ = new Employes();

    public static Employes getEmployes() {
        return employ;
    }

    public Employes initEmployes(String name, char sex, int age) {
        employ.setName(name);
        employ.setSex(sex);
        employ.setAge(age);
        return employ;
    }

    private Employes() {}

    public void examineEnclots(Enclots enclot) {
        enclot.getInfos();
    }

    public void cleanEnclots(Enclots enclot) {
        enclot.clean();
        System.out.println(this.name + " a nettoy� l'enclos " + enclot.getName());
    }

    public void feedAnimalsOfEnclots(Enclots enclot) {
        enclot.feedAnimals();
        System.out.println(this.name + " a nourri tous les animaux de l'enclos " + enclot.getName());
    }

    public void moveAnimalFromEnclots(Animal animal, Enclots enclot1, Enclots enclot2) {
        if(enclot1.removeAnimal(animal)) {
            if(enclot2.addAnimal(animal)) {
                System.out.println(this.name + " a d�plac� l'animal " + animal.getName() + " de l'enclos " + enclot1.getName() + " a l'enclos " + enclot2.getName());
            } else {
                enclot1.addAnimal(animal);
            }
        }
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSex(char sex) {
        this.sex = sex;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
