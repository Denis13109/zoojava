package Enclot;

import Animal.Animal;
import Animal.AnimalMarin;

public class Aquarium <T extends AnimalMarin> extends Enclots {
    private int profondeur;
    private int propretee;

    public Aquarium(String name, int area, int maxAnimal, int profondeur) {
        super(name, area, maxAnimal);
        this.profondeur = profondeur;
        this.propretee = 1;
    }
}
