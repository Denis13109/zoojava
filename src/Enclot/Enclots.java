package Enclot;

import Animal.Animal;

import java.util.ArrayList;

public abstract class Enclots <T> {
    private String name;
    private int surface;
    private ArrayList<T> animals;
    private int propretee; 
    private int maxAnimal; 
    private int nbAnimal; 

    public Enclots(String name, int surface, int maxAnimal) {
        this.name = name;
        this.surface = surface;
        this.maxAnimal = maxAnimal;
        this.nbAnimal = 0;
        this.animals = new ArrayList<T>();
        this.propretee = 1;
    }

    public void getInfos() {
        System.out.println("Enclos : "+ this.name);
        System.out.println("Superficie " + this.surface);
        System.out.println("Nombre d'animaux miximum : " + this.maxAnimal);
        System.out.println("Nombre d'animaux actuel : " + this.nbAnimal);
        System.out.println("Niveau de propret� : " + this.propretee + "/3");
        for(T animal : animals) {
            System.out.println(animal);
        }
    }
    
    public void clean() {
        this.propretee = 1;
        System.out.println("L'enclos " + this.name + " est tout propre ca yest");
    }
    
    public boolean removeAnimal(T animal) {
        if(this.animals.contains(animal)) {
            this.animals.remove(animal);
            --this.nbAnimal;
           System.out.println(((Animal) animal).getName() + " s'est fais retir� de l'enclos " + this.name);
        } else {
            System.out.println("L'animal se trouve pas dans l'enclos " + this.name);
            return false;
        }
        return true;
    }
    
    public boolean addAnimal(T animal) {
        if(this.nbAnimal + 1 <= this.maxAnimal) {
            if(!this.animals.contains(animal)) {
                this.animals.add(animal);
                ++this.nbAnimal;
                System.out.println(((Animal) animal).getName() + " a �t� ajout� � l'enclos " + this.name + " ("+ this.nbAnimal +"/"+ this.maxAnimal +")");
            } else {
                System.out.println(((Animal) animal).getName() + " est d�j� dans l'enclos " + this.name);
                return false;
            }
        } else {
            System.out.println("Dans l'enclot " + this.name + " le nb max d'animals est atteint !");
            return false;
        }
        return true;
    }

    public void feedAnimals() {
        for(T animal : animals) {
            ((Animal) animal).eat();
        }
        System.out.println("Dans l'enclot "+ this.name + " sont tous rassasi�");
    }

    public void showAnimals() {
        
    }

    public String getName() {
        return name;
    }

    public int getNbAnimal() {
        return nbAnimal;
    }

    public ArrayList<T> getAnimals() {
        return animals;
    }
}
