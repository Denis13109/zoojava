package Enclot;

import Animal.Animal;
import Animal.AnimalVolant;

public class Voliere <T extends AnimalVolant> extends Enclots {
    private int height;
    
    public Voliere(String name, int area, int maxAnimal, int height) {
        super(name, area, maxAnimal);
        this.height = height;
    }
}
