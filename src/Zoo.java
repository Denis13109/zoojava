
import Animal.Animal;
import Employe.Employes;
import Enclot.Enclots;

import java.util.ArrayList;

// Design Pattern : Singleton
public final class Zoo {
    private String name;
    private Employes employe;
    private int maxEnclots;
    private ArrayList<Enclots> enclot;
    private static Zoo ZOO = new Zoo();

    public static Zoo getZoo() {
        return ZOO;
    }

    public Zoo initZoo(String name, Employes employe, int maxEnclots) {
        ZOO.setName(name);
        ZOO.setEmployes(employe);
        ZOO.setMaxEnclots(maxEnclots);
        ZOO.setEnclots(new ArrayList<Enclots>());
        return ZOO;
    }

    private Zoo() { }

    public void addEnclot(Enclots enclot) {
        this.enclot.add(enclot);
        System.out.println("L'enclos " + enclot.getName() + " a �t� ajout� au Zoo " + this.name);
    }

    public void showNbAnimals() {
        int nbAnimals = 0;
        for(Enclots enclot : this.enclot) {
            nbAnimals += enclot.getNbAnimal();
        }
        System.out.println("Le zoo " + this.name + " a " + nbAnimals + " animaux");
    }

    public void showAllAnimals() {
        System.out.println("=== Tous les animaux du zoo " + this.name + " ===");
        for(Enclots enclot : this.enclot) {
            System.out.println("Animaux de l'enclos " +  enclot.getName() +  " : " + enclot.getAnimals());
        }
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMaxEnclots(int maxEnclots) {
        this.maxEnclots = maxEnclots;
    }

    public void setEnclots(ArrayList<Enclots> enclots) {
        this.enclot = enclots;
    }

    public void setEmployes(Employes employe) {
        this.employe = employe;
    }
}
