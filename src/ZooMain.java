import Animal.*;
import Employe.Employes;
import Enclot.Enclots;
import Enclot.EnclotBasic;
import Enclot.Aquarium;
import Enclot.Voliere;


public class ZooMain {
    public static void main(String [ ] args) {
        Employes employ = Employes.getEmployes().initEmployes("Marc LAPORTE", 'm', 89);
        Zoo zoo = Zoo.getZoo().initZoo("Zoo++", employ, 5);

        Enclots<Loups> enclot = new EnclotBasic<Loups>("For�t des loups", 42, 2);
        Enclots<Tigres> enclot2 = new EnclotBasic<Tigres>("Famille Tigrou", 56, 1);
        Enclots<PoissonsRouge> enclot3 = new Aquarium<PoissonsRouge>("Aquarium des petits poissons", 58,3, 5);
        Enclots<Aigles> enclot4 = new Voliere<Aigles>("Voli�re volante", 60, 5, 35);
        Enclots<PoissonsRouge> enclot5 = new Aquarium<PoissonsRouge>("Petit aquarium", 20, 1, 3);

        Loups wolf = new Loups("Rex", 'm', 48, 62);
        Loups wolf2 = new Loups("Lina", 'f', 41, 59);
        Loups wolf3 = new Loups("Dextructor", 'm', 481, 160);
        Tigres tiger = new Tigres("Tigrou", 'm', 62, 189);
        PoissonsRouge goldFish = new PoissonsRouge("Nemo", 'm', 1, 2);
        PoissonsRouge goldFish2 = new PoissonsRouge("Marin", 'm', 1, 3);
        PoissonsRouge goldFish3 = new PoissonsRouge("Dory", 'f', 1,3);
        Aigles eagle = new Aigles("Royal", 'f', 5, 2);

        zoo.setEmployes(employ);
        zoo.addEnclot(enclot);
        zoo.addEnclot(enclot2);
        zoo.addEnclot(enclot3);
        zoo.addEnclot(enclot4);
        zoo.addEnclot(enclot5);

        enclot.addAnimal(wolf);
        enclot.addAnimal(wolf2);
        enclot2.addAnimal(tiger);
        enclot2.addAnimal(tiger);
        enclot3.addAnimal(goldFish);
        enclot3.addAnimal(goldFish2);
        enclot3.addAnimal(goldFish3);
        enclot3.feedAnimals();
        enclot4.addAnimal(eagle);

        employ.moveAnimalFromEnclots(goldFish2, enclot3, enclot5);
        employ.moveAnimalFromEnclots(goldFish, enclot5, enclot3);

        zoo.showAllAnimals();

        enclot.feedAnimals();

    }
}